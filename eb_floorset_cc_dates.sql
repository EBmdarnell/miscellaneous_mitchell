create table eb_floorset_cc_dates diststyle even as
(
select
a.dept_style_color
,min(a.bow_date) as cc_start_date
,max(a.bow_date) as cc_end_date

from
eb_assortment a

group by
a.dept_style_color
)
;