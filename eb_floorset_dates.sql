create table eb_floorset_dates diststyle even as
(
select
floorset
,fiscal_year
,min(a.bow_date) as floorset_start_date
,max(a.bow_date) as floorset_end_date

from
eb_assortment a

group by
floorset
,fiscal_year
)
;