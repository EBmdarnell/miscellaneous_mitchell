select

a.dept_id as old_dept_id,
case when a.business_id in (1,3,11) then 'US' else 'CA' end as Country,
cast(a.dept_id as varchar(2))+cast(a.style_id as varchar(4)) as Dept_Style,
cast(a.dept_id as varchar(2))+cast(a.style_id as varchar(4))+cast(a.color_id as varchar(3)) as Dept_Style_Color,


cast(a.price_type_flag as varchar(1)) as price_type_flag,
b.fiscal_year,
b.fiscal_month,
b.fiscal_week,
b.fiscal_day,

c.comp_fiscal_year,
c.comp_fiscal_month,
c.comp_fiscal_week,

a.tran_date,
 
l.location_id,
 
case when l.location_id in (590,591) then 12 else l.business_id end as business_id,
case when l.location_type_id in (777,999) then 'DC' else 'Store' end as dc_or_store,
case when m."2018 sum assort" = 'Direct' then 'D-Core' 
    when m."2018 sum assort" = 'Direct Only' then 'D-Add'
    when m."2018 sum assort" = 'Base Camp' then 'Base'
    when m."2018 sum assort" = 'Camp 1' then 'Camp'
    else m."2018 sum assort" end as store_group,
 
cast(m.region as varchar(10)),
m.district,
cast(m.climate as varchar(10)),
m."store name",
cast(m."comp status" as varchar(10)),
m.R#,
m.L#,
m."fp-outlet",
 

cast(st.division_name as varchar(20)) as Division_name,

st.dept_name as Category_name,
st.class as New_Dept_id,
st.class_name as New_Dept_name,

st.subclass_name as New_Class_name,
cast(st.series as varchar(30)) as Series,
ex.exchange_rate,

st.group_name as group_name,
a.color_id,
a.style_id,
a.size_id,
cast(lkup.color_abbr as varchar(15))as color_name,
cast(lkup.style_name as varchar(30)) as style_name,
cast(lkup.size_abbr as varchar(8)) as size_name,

a.category,
st.new_dept_id as category_id,
st.group_number as group_id,
st.subclass as new_class_id,
 
sum(a.tran_units) as "Sales Units",
sum(a.retail_cash_amt*isnull(ex.exchange_rate,1)) as Retail_Cash_Amt,
sum(a.retl_std_ext*isnull(exchange_rate,1)) as "Ticket Price",
sum(a.retl_temp_ext*isnull(exchange_rate,1)) as Retl_Temp_Ext
 
 
from
eb_retailitemsales a
 
left join eb_fiscal_date b
on b.actual_date = a.tran_date
 
left join eb_exchange_rate_hist ex
on ex.fiscal_year = b.fiscal_year
and ex.fiscal_month = b.fiscal_month
and ex.fiscal_week = b.fiscal_week
and ex.nation_id = 'CA'
and a.business_id in (2,4)
 
left join eb_rms_styles st
on a.dept_id = st.dept_id
and a.style_id = st.style_id_alpha*1
 
left join eb_comp_fiscal_date_weekly c
on a.fiscal_year = c.fiscal_year
and a.fiscal_month = c.fiscal_month
and a.fiscal_week = c.fiscal_week
 
left join eb_msp m
on a.location_id = m.l#
 
left join eb_location_mstr l
on a.location_id = l.location_id

left join eb_sku_lkup lkup
on a.color_id = lkup.color_id
and a.size_id = lkup.size_id
and a.style_id = lkup.style_id
and a.dept_id = lkup.dept_id
 
 
group by


a.dept_id,
case when a.business_id in (1,3,11) then 'US' else 'CA' end,
cast(a.dept_id as varchar(2))+cast(a.style_id as varchar(4)),
cast(a.dept_id as varchar(2))+cast(a.style_id as varchar(4))+cast(a.color_id as varchar(3)),




cast(a.price_type_flag as varchar(1)),
b.fiscal_year,
b.fiscal_month,
b.fiscal_week,
b.fiscal_day,
c.comp_fiscal_year,
c.comp_fiscal_month,
c.comp_fiscal_week,

a.tran_date,
 
l.location_id,
 
l.business_id,
l.location_type_id,
m."2018 sum assort",
 
 
cast(m.region as varchar(10)),

m.district,
cast(m.climate as varchar(10)),
m."store name",
cast(m."comp status" as varchar(10)),
m.R#,
m.L#,
m."fp-outlet",
 

cast(st.division_name as varchar(20)),

st.dept_name,
st.class,
st.class_name,

st.subclass_name,
cast(st.series as varchar(30)),
ex.exchange_rate,

st.group_name,
a.color_id,
a.style_id,
a.size_id,
cast(lkup.color_abbr as varchar(15)),
cast(lkup.style_name as varchar(30)),
cast(lkup.size_abbr as varchar(8)),
a.category,
st.new_dept_id,
st.group_number,
st.subclass