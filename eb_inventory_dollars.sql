create table eb_inventory_dollars diststyle even as
  (
SELECT [business_id], 
       salesinv.[fiscal_year], 
       salesinv.[fiscal_month], 
       salesinv.[fiscal_week],      
       salesinv.[dept_id]                                      AS Old_dept_id, 
       salesinv.[style_id], 
       salesinv.[color_id], 
       salesinv.[size_id], 
       [units_owned_bow], 
       [cost_owned_bow] * Isnull([exchange_rate], 1)           AS [Cost_owned_BOW], 
       [retl_owned_bow] * Isnull([exchange_rate], 1)           AS [Retl_owned_BOW], 
       [style_desc], 
       st.group_number                                         AS Group_id, 
       st.group_name                                           AS Group_name, 
       st.division                                             AS Division_id, 
       st.division_name                                        AS Division_name, 
       st.new_dept_id                                          AS Category_id, 
       st.dept_name                                            AS Category_name, 
       st.class                                                AS New_Dept_id, 
       st.class_name                                           AS New_Dept_name, 
       st.subclass                                             AS New_Class_id, 
       st.subclass_name                                        AS New_Class_name, 
       st.series                                               AS Series, 

 

       Cast(salesinv.dept_id AS VARCHAR(2)) + '-' + Cast(salesinv.style_id AS VARCHAR(4)) + '-' + Cast(salesinv.color_id AS VARCHAR(4))                 AS "Old_Dept-Style-Color", 

       Cast(st.class AS VARCHAR(2)) + '-' + Cast(salesinv.style_id AS VARCHAR(4))           AS "New_Dept-Style", 

       Cast(st.class AS VARCHAR(2)) + '-' + Cast(salesinv.style_id AS VARCHAR(4)) + '-' + Cast(salesinv.color_id AS VARCHAR(4))                 AS "New_Dept-Style-Color", 

       Cast(salesinv.dept_id AS VARCHAR(2)) + '-' + Cast(salesinv.style_id AS VARCHAR(4))                 AS "Old_Dept-Style",

       Cast(Cast(salesinv.fiscal_year AS VARCHAR(4)) 
            + RIGHT('000'+Cast(salesinv.fiscal_month AS VARCHAR(2)), 2) 
            + Cast(salesinv.fiscal_week AS VARCHAR(1)) AS INT) AS FiscalYMW, 

       salesinv.locationtype as DC_or_store,
Isnull([exchange_rate], 1) as exchange_rate
       
FROM   eb_wk_sku_bus_inv salesinv 
       Left JOIN (SELECT 
      orig.[Fiscal_Year]
      ,orig.[Fiscal_Month]
      ,orig.[Fiscal_Week]
      ,shift.[Fiscal_Year] as yearshift
      ,shift.[Fiscal_Month] as monthshift
      ,shift.[Fiscal_Week] as weekshift
  FROM (SELECT cast([ACTUAL_DATE] as DATE) as ACTUAL_DATE,[FISCAL_YEAR],[FISCAL_MONTH],[FISCAL_WEEK] FROM eb_fiscal_date where FISCAL_DAY=7) orig
  join  (SELECT cast([ACTUAL_DATE] as DATE) as ACTUAL_DATE,[FISCAL_YEAR],[FISCAL_MONTH],[FISCAL_WEEK] FROM eb_fiscal_date where FISCAL_DAY=7) shift
  on orig.actual_date = Dateadd(week,-1,shift.actual_date)
   ) weekshift
      on salesinv.fiscal_year  = weekshift.yearshift
      and salesinv.fiscal_month  = weekshift.monthshift
      and salesinv.fiscal_week = weekshift.weekshift
      
      
       
       LEFT JOIN eb_exchange_rate_hist ex 
              ON ex.fiscal_year = weekshift.fiscal_year 
                 AND ex.fiscal_month = weekshift.fiscal_month 
                 AND ex.fiscal_week = weekshift.fiscal_week 
                 AND ex.nation_id = 'CA' 
                 AND salesinv.business_id IN ( 2, 4, 12 )
                 
       LEFT JOIN eb_rms_styles st 
              ON salesinv.dept_id = st.dept_id 
                 AND salesinv.style_id = st.style_id_alpha

                 );
  GRANT SELECT ON ALL TABLES IN SCHEMA PUBLIC TO eb_read;