import csv
from pyodbc import connect
import os
from datetime import datetime,date,timedelta
import time
import pandas as pd
import boto3
import sys 

def write_csv(cursor,file):
	with open('files/%s.csv' % file,'w', encoding='UTF-8',newline='') as csv_file:
		csv_writer = csv.writer(csv_file)
		csv_writer.writerow(i[0] for i in cursor.description)
		csv_writer.writerows(cursor)
	size = os.stat('files/%s.csv' % file).st_size
	return size

def get_cursor(db):
	conn = connect(driver='{SQL Server}', server=db, trusted_connection='yes')
	cursor = conn.cursor()
	return cursor

def get_prior_day(interval,hist=1):
	if interval == 'daily':
		shift = 1
	else:
		shift = 6
	yesterday = date.today()- timedelta(shift*hist) #historical loads hist != 1
	SQL = """SELECT [FISCAL_YEAR],[FISCAL_MONTH],[FISCAL_WEEK],[FISCAL_DAY]
	FROM [epitemmvs].[dbo].[FISCAL_DATE]
	WHERE ACTUAL_DATE = '%s'""" % yesterday.strftime('%Y-%m-%d')

	cursor = get_cursor('WMSQLPRD01')
	cursor.execute(SQL)
	row = cursor.fetchone()
	return row[0],row[1],row[2],row[3]

def open_sql(file,fiscal_ymwd):
	with open("sql/%s.sql" % file,'r') as f:
		sql = f.read()
		sql = sql.replace('*fiscal_year*',str(fiscal_ymwd[0]))
		sql = sql.replace('*fiscal_month*',str(fiscal_ymwd[1]))
		sql = sql.replace('*fiscal_week*',str(fiscal_ymwd[2]))
		sql = sql.replace('*fiscal_day*',str(fiscal_ymwd[3]))
	return sql

def file_to_csv(file):
	file_type = file.split('.')[-1]
	file_name = file.split('\\')[-1].split('.')[0]
	if file.find('assortment') > -1:
		s3_table_name = 'assortment'
	else:
		s3_table_name = file_name
	
	if file_type == 'xlsx':
		data = pd.read_excel(file,'Sheet1')
		data.to_csv('files/%s.csv' % file_name,index=False)
	elif file_type == 'csv':
		os.system('copy % s C:\\s3\\files\\%s.csv' % (file,file_name))
	else:
		asdf
	print(file_type)
	print(file_name)
	size = os.stat('files/%s.csv' % file_name).st_size
	return s3_table_name,file_name,size

os.chdir('C:\\s3')
s3 = boto3.resource('s3')
interval = sys.argv[1]
run_info = pd.read_csv('run_info.csv')
fiscal_ymwd = get_prior_day(interval)
for i,r in run_info.iterrows():
	if (r['interval'] == interval):
		print('Loading %s for %s' % (r['table'],fiscal_ymwd))
		if (r['format'] == 'file'):
			s3_table_name,file_name,size = file_to_csv(r['table'])
		else:
			cursor = get_cursor(r['server'])
			SQL = open_sql(r['table'],fiscal_ymwd)
			cursor.execute(SQL)
			size = write_csv(cursor,r['table'])
			file_name = r['table']
			s3_table_name = r['table']
		if size > 2000:	
			#This would be faster with aws cp, but couldn't get os.system aws call to work when scheduling
			with open('files/%s.csv' % file_name,'rb') as f:		
				if r['type'] == 'full':			
					s3.Bucket('eb-inbox').put_object(Key='internal/%s/%s.csv' % (s3_table_name,file_name),Body=f)
				elif r['interval'] == 'weekly':
					s3.Bucket('eb-inbox').put_object(Key='internal/%s/%s_%s_%s_%s.csv' % (s3_table_name,file_name,fiscal_ymwd[0],fiscal_ymwd[1],fiscal_ymwd[2]),Body=f)
				elif r['interval'] == 'daily':
					s3.Bucket('eb-inbox').put_object(Key='internal/%s/%s_%s_%s_%s_%s.csv' % (s3_table_name,file_name,fiscal_ymwd[0],fiscal_ymwd[1],fiscal_ymwd[2],fiscal_ymwd[3]),Body=f)
				else:
					print('%s not uploaded with size %s bytes' % (r['table'],size))
			os.remove('files/%s.csv' % file_name)
		else:
			print('Skipping %s for %s, weekday = %s' % (r['table'],fiscal_ymwd,date.today().weekday()))

# r={}
# interval = 'daily'
# r['table'] = 'RetailItemSales'
# r['server'] = 'WMSQLPRD12'
# r['interval'] = 'daily'
# r['type'] = 'incremental'
# r['format'] = 'sql'
# for i in range(1,600):
# 	fiscal_ymwd = get_prior_day(interval,i)
# 	if (r['interval'] == interval):
# 		print('Loading %s for %s' % (r['table'],fiscal_ymwd))
# 		if (r['format'] == 'file'):
# 			s3_table_name,file_name,size = file_to_csv(r['table'])
# 		else:
# 			cursor = get_cursor(r['server'])
# 			SQL = open_sql(r['table'],fiscal_ymwd)
# 			cursor.execute(SQL)
# 			size = write_csv(cursor,r['table'])
# 			file_name = r['table']
# 			s3_table_name = r['table']
# 		if size > 2000:	
# 			#This would be faster with aws cp, but couldn't get os.system aws call to work when scheduling
# 			with open('files/%s.csv' % file_name,'rb') as f:		
# 				if r['type'] == 'full':			
# 					s3.Bucket('eb-inbox').put_object(Key='internal/%s/%s.csv' % (s3_table_name,file_name),Body=f)
# 				elif r['interval'] == 'weekly':
# 					s3.Bucket('eb-inbox').put_object(Key='internal/%s/%s_%s_%s_%s.csv' % (s3_table_name,file_name,fiscal_ymwd[0],fiscal_ymwd[1],fiscal_ymwd[2]),Body=f)
# 				elif r['interval'] == 'daily':
# 					s3.Bucket('eb-inbox').put_object(Key='internal/%s/%s_%s_%s_%s_%s.csv' % (s3_table_name,file_name,fiscal_ymwd[0],fiscal_ymwd[1],fiscal_ymwd[2],fiscal_ymwd[3]),Body=f)
# 				else:
# 					print('%s not uploaded with size %s bytes' % (r['table'],size))
# 			os.remove('files/%s.csv' % file_name)
# 		else:
# 			print('Skipping %s for %s, weekday = %s' % (r['table'],fiscal_ymwd,date.today().weekday()))