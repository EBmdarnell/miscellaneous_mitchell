WITH agg_cc AS (
  SELECT 
  fiscal_year,
  fiscal_month,
  fiscal_week,
  fiscalymw,
  "l#",
  --business_id,
  --"new_dept-style",
  --"new_dept-style-color",
  --style_id,
  color_id,
  --style_name,
  --regexp_replace(style_name, '^(M|W)(P|R|T) ') as style_name_wo_e,
  --color_name,
  --style_desc,
  --color_desc,
  group_id,
  --group_name,
  division_id,
  --division_name,
  category_id,
  --category_name,
  new_dept_id,
  --new_dept_name,
  --new_class_id,
  --new_class_name,
  --series,
  --dc_or_store,
  --date,
  --comp_fiscal_year,
  --comp_fiscal_month,
  --comp_fiscal_week,
  --exchange_rate,
  "r#",
  --climate,
  --country,
  --district,
  --region,
  --"fp-outlet",
  --"comp status",
  --"store name",
  --longitude,
  --latitude,
  --state,
  --salespricetype,
  assortment_group,
  drn,
  --dev,
  assortment_season,
  store_group,
  --store_net_sqft,
  assorted_flag,
  --assorted_sku_loc_count,
  floorset,
  SUM(eoh_instore) eoh_instore,
  SUM(eoh_instore_cost) eoh_instore_cost,
  SUM(net_sales) net_sales,
  SUM(net_cost) net_cost
  FROM public.eb_wk_sku_loc_mstr m
  WHERE 1=1
    --AND m.fiscal_year = 2018
    --AND m.floorset IN ('FAL 1', 'FAL 2', 'FAL 3')
    AND m.division_name IS NOT NULL
    AND m.assortment_group <> 'Direct Only'
  GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
)
, agg_loc AS (
  SELECT
  fiscal_year,
  fiscal_month,
  fiscal_week,
  fiscalymw,
  "l#",
  group_id,
  division_id,
  category_id,
  new_dept_id,  
  "r#",
  assortment_group,
  assortment_season,
  store_group,
  assorted_flag,
  floorset,
  COUNT(DISTINCT TO_CHAR(new_dept_id,'varchar') + '-' + drn + '-' + TO_CHAR(color_id,'varchar')) countd_cc,
  AVG(CAST(eoh_instore AS DECIMAL(38, 19))) avg_eoh_instore_per_cc,
  AVG(CAST(eoh_instore_cost AS DECIMAL(38, 19))) avg_eoh_instore_cost_per_cc,
  SUM(CAST(net_sales AS DECIMAL(38, 19))) sum_net_sales_per_cc,
  SUM(CAST(net_sales AS DECIMAL(38, 19))) - SUM(CAST(net_cost AS DECIMAL(38, 19))) sum_gross_margin_per_cc
  FROM agg_cc
  GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
)
, agg_sg AS (
  SELECT
  fiscal_year,
  fiscal_month,
  fiscal_week,
  fiscalymw,
  group_id,
  division_id,
  category_id,
  new_dept_id, 
  assortment_group,
  assortment_season,
  store_group,
  assorted_flag,
  floorset,
  SUM(CAST(countd_cc AS DECIMAL(38, 19)))/COUNT("r#") sg_cc_avg_per_loc,
  SUM(CAST(avg_eoh_instore_per_cc AS DECIMAL(38, 19)))/COUNT("r#") sg_cc_units_avg_per_loc,
  SUM(CAST(avg_eoh_instore_cost_per_cc AS DECIMAL(38, 19)))/COUNT("r#") sg_cc_eoh_instore_cost_avg_per_loc,
  SUM(CAST(sum_net_sales_per_cc AS DECIMAL(38, 19)))/COUNT("r#") sg_cc_net_sales_avg_per_loc,
  SUM(CAST(sum_gross_margin_per_cc AS DECIMAL(38, 19)))/COUNT("r#") sg_cc_gross_margin_avg_per_loc
  FROM agg_loc
  GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13
)
SELECT 
m.fiscal_year,
m.fiscal_month,
m.fiscal_week,
m.fiscalymw,
--"l#",
m.business_id,
m."new_dept-style",
m."new_dept-style-color",
m.style_id,
m.color_id,
m.style_name,
regexp_replace(m.style_name, '^(M|W)(P|R|T) ') as style_name_wo_e,
m.color_name,
--style_desc,
--color_desc,
m.group_id,
m.group_name,
m.division_id,
m.division_name,
m.category_id,
m.category_name,
m.new_dept_id,
m.new_dept_name,
m.new_class_id,
m.new_class_name,
m.series,
m.dc_or_store,
--date,
m.comp_fiscal_year,
m.comp_fiscal_month,
m.comp_fiscal_week,
--exchange_rate,
--"r#",
--climate,
--country,
--district,
--region,
m."fp-outlet",
--"comp status",
--"store name",
--longitude,
--latitude,
--state,
--salespricetype,
m.assortment_group,
m.drn,
--dev,
m.assortment_season,
m.store_group,
--store_net_sqft,
m.assorted_flag,
--assorted_sku_loc_count,
m.floorset,
t.department_threshold,
t.store_group_threshold,
COUNT("l#") "l#_count",
COUNT("r#") "r#_count",
SUM(eoh_booked_cost) eoh_booked_cost,
SUM(eoh_booked_retail) eoh_booked_retail,
SUM(eoh_instore_cost) eoh_instore_cost,
SUM(eoh_instore_retail) eoh_instore_retail,
SUM(eoh_booked_cost_gmroii) eoh_booked_cost_gmroii,
SUM(eoh_instore_cost_gmroii) eoh_instore_cost_gmroii,
SUM(net_clearance_units) net_clearance_units,
SUM(eoh_booked) eoh_booked,
SUM(eoh_instore) eoh_instore,
SUM(net_sales) net_sales,
SUM(net_units) net_units,
SUM(gross_units) gross_units,
SUM(net_cost) net_cost,
SUM(gross_cost) gross_cost,
SUM(net_empdiscount) net_empdiscount,
SUM(gross_ticket) gross_ticket,
SUM(net_ticket) net_ticket,
AVG(sg_cc_avg_per_loc) sg_cc_avg_per_loc,
AVG(sg_cc_units_avg_per_loc) sg_cc_units_avg_per_loc,
AVG(sg_cc_eoh_instore_cost_avg_per_loc) sg_cc_eoh_instore_cost_avg_per_loc,
AVG(sg_cc_net_sales_avg_per_loc) sg_cc_net_sales_avg_per_loc,
AVG(sg_cc_gross_margin_avg_per_loc) sg_cc_gross_margin_avg_per_loc
FROM public.eb_wk_sku_loc_mstr m
LEFT JOIN agg_sg sg ON
  m.fiscal_year = sg.fiscal_year
  AND m.fiscal_month = sg.fiscal_month
  AND m.fiscal_week = sg.fiscal_week
  AND m.fiscalymw = sg.fiscalymw
  AND m.group_id = sg.group_id
  AND m.division_id = sg.division_id
  AND m.category_id = sg.category_id
  AND m.new_dept_id = sg.new_dept_id
  AND m.assortment_group = sg.assortment_group
  AND m.assortment_season = sg.assortment_season
  AND m.store_group = sg.store_group
  AND m.assorted_flag = sg.assorted_flag
  AND m.floorset = sg.floorset
LEFT JOIN public.eb_redshift_thresholds t ON
  m.group_id = t.group_id
  AND m."fp-outlet" = t.fp_outlet
  AND m.assortment_group = t.assortment_group
WHERE 1=1
  --AND m.fiscal_year = 2018
  --AND m.floorset IN ('FAL 1', 'FAL 2', 'FAL 3')
  AND m.division_name IS NOT NULL
  AND m.assortment_group <> 'Direct Only'
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36